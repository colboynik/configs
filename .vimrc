set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set pastetoggle=<F2>

set listchars=tab:>-,trail:-
set list

let mapleader =  ","

noremap <Leader>e :quit<CR>
noremap <Leader>E :qa!<CR>

" map sort function to a key
vnoremap <Leader>s :sort<CR>

" easier exiting of insert mode
imap kj <esc>
imap jj <esc>
imap jk <esc>

" Show whitespace
" Must be before colorscheme command
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
au InsertLeave * match ExtraWhitespace /\s\+$/

" Color scheme
" mkdir -p ~/.vim/colors && cd ~/.vim/colors
" wget -O wombat256mod.vim http://www.vim.org/scripts/download_script.php?src_id=13400
set t_Co=256
color wombat256mod

" Enable syntax highlighting
filetype plugin indent on
set omnifunc=syntaxcomplete#Complete
syntax on

set number
"set tw=79
set nowrap
set fo-=t " do not automatically wrap text when typing
" set colorcolumn=80
highlight ColorColumn ctermbg=2

" bind Ctrl+<movement> keys to move around the windows, instead of using Ctrl+w
" + <movement>
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" easier moving between tabs
map<Leader>n <esc>:tabprevious<CR>
map<Leader>m <esc>:tabnext<CR>

" easier moving of code blocks
vnoremap < <gv  " better indentation
vnoremap > >gv  " better indentation

" Setup Pathogen to manage plugins
" mkdir -p ~/.vim/autoload ~/.vim/bundle
" curl -Sso ~/.vim/autoload/pathogen.vim https://raw.github.com/tpope/vim-pathogen/master/autoload/pathogen.vim
" Now you can install any plugin into a .vim/bundle/plugin-name/folder
" call pathogen#infect()
" call pathogen#helptags()

" ==========================
" Python IDE Setup
" ==========================

" Settings for vim-powerline
" cd ~/.vim/bundle
" git clone git://github.com/Lokaltog/vim-powerline.git
set laststatus=2

" Settings for ctrlp
" cd ~/.vim/bundle
" git clone https://github.com/kien/ctrlp.vim.git
" let g:ctrlp_max_height = 30
" set wildignore+=#.pyc
" set wildignore+=$_build/*
" set wildignore+=*/coverage/*

" Settings for jedi-vim
" cd ~/.vim/bundle
" git clone https://github.com/davidhalter/jedi-vim.git
" sudo pip install jedi
" let g:jedi#completions_command = '<Leader>c' " Testing
" Ctrl-Space for completions. Heck Yeah!
" inoremap <C-Space> <C-X><C-O>

" Settings for syntastic
" cd ~/.vim/bundle
" git clone https://github.com/scrooloose/syntastic.git
" pip install pylint
" pip install pyflakes
let g:syntastic_auto_loc_list = 1
let g:syntastic_python_checkers=['pylint']
let g:syntastic_always_populate_loc_list=1

" Better navigating through omnicomplete option list
" See http://stackoverflow.com/questions/2170023/how-to-map-keys-for...
" set completeopt=longest,menuone
" function! OmniPopup(action)
"    if pumvisible()
"        if a:action == 'j'
"                return "\<C-N>"
"        elseif a:action == 'k'
"            return "\<C-P>"
"        endif
"    return a:action
" ndfunction
"
" noremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
" noremap <silent><C-k> <C-R>=OmniPopup('k')<CR>
"
" Python folding
" mkdir -p ~/.vim/ftplugin
" wget -O ~/.vim/ftplugin/python_editing.vim http://www.vim.org/script
" wget above is incomplete, can't find the right one
" set nofoldenable
"
:map <F3> :source $HOME/.vim/scripts/comma_list

autocmd Filetype java set makeprg=javac\ %
map <F9> :make<Return>:copen<Return>
map <F10> :cprevious<Return>
map <F11> :cnext<Return>

" F9/F10 compile/run default file.
" F11/F12 compile/run alternate file.

map <F9> :set makeprg=javac\ %<CR>:make<CR>
map <F10> :!echo %\|awk -F. '{print $1}'\|xargs java<CR>
map <F11> :set makeprg=javac\ #<CR>:make<CR>
map <F12> :!echo #\|awk -F. '{print $1}'\|xargs java<CR>

map! <F9> <Esc>:set makeprg=javac\ %<CR>:make<CR>
map! <F10> <Esc>:!echo %\|awk -F. '{print $1}'\|xargs java<CR>
map! <F11> <Esc>set makeprg=javac\ #<CR>:make<CR>
map! <F12> <Esc>!echo #\|awk -F. '{print $1}'\|xargs java<CR>

" Tip: load a file into the default buffer, and its driver
" into the alternate buffer, then use F9/F12 to build/run.
" Note: # (alternate filename) isn't set until you :next to it!
" Tip2: You can make then run without hitting ENTER to continue. F9-F12

" With these you can cl/cn/cp (quickfix commands) to browse the errors
" after you compile it with :make

set makeprg=javac\ %
set errorformat=%A:%f:%l:\ %m,%-Z%p^,%-C%.%#
" au FileType xml exe ":silent %!xmllint --format --recover - 2>/dev/null"
autocmd FileType html setlocal shiftwidth=2 tabstop=2
autocmd FileType xml setlocal shiftwidth=2 tabstop=2
